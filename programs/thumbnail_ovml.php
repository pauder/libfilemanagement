<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

bab_functionality::includeFile('Ovml/Function');



/**
 * Create a thumbnail image
 *
 * <OFThumbnail
 * 		( id_directory_entry="" | id_user="" | id_article="" | path="absolute/path/to/image" )
 * 		[maxwidth="400"]
 * 		[maxheight="400"]
 * 		[resizemode="KEEP_ASPECT_RATIO | CROP_CENTER"]
 * 		[default="path/to/default/image.jpg"]
 * 		[innerborderwidth="0"]
 * 		[innerbordercolor="#ffffff"]
 * 		[innerborder="0,#ffffff"]
 * 		[outerborderwidth="0"]
 * 		[outerbordercolor="#ffffff"]
 * 		[outerborder="0,#ffffff"]
 *      [resizeleft="0.0 .. 1.0"]
 *      [resizetop="0.0 .. 1.0"]
 * 		[saveas=""]
 * >
 *
 */
class Func_Ovml_Function_Thumbnail extends Func_Ovml_Function
{

    /**
     * List of attribute not managed by the ovml function
     * used by OFImg
     *
     * @var array
     */
    protected $externalAttributes = array();
    protected $computedWidth = null;
    protected $computedHeight = null;

    public function toString()
    {
        $T = bab_functionality::get('Thumbnailer');
        /*@var $T Func_Thumbnailer */


        if (!$T || !count($this->args))
        {
            return '';
        }


        $maxwidth = 400;
        $maxheight = 400;
        $innerbordercolor = '#ffffff';
        $outerbordercolor = '#cccccc';
        $innerborderwidth = 0;
        $outerborderwidth = 0;
        $resizeleft = 0.5;
        $resizetop = 0.5;
        $outerborderwidth = 0;
        $resizemode = Func_Thumbnailer::KEEP_ASPECT_RATIO;
        $default = null;
        $saveas = null;
        $imageUrl = null;
        $sourceMethod = null;
        $sourceValue = null;

        foreach ($this->args as $p => $v) {

            $p = mb_strtolower(trim($p));

            switch ($p) {

                case 'maxwidth':

                    if ($v <= 0)
                    {
                        trigger_error(sprintf('Invalid parameter maxwidth=%s in OFThumbnail', $v));
                        break;
                    }

                    $maxwidth = $v;
                    break;

                case 'maxheight':

                    if ($v <= 0)
                    {
                        trigger_error(sprintf('Invalid parameter maxheight=%s in OFThumbnail', $v));
                        break;
                    }

                    $maxheight = $v;
                    break;

                case 'innerborderwidth':
                    $innerborderwidth = $v;
                    break;
                case 'innerbordercolor':
                    $innerbordercolor = $v;
                    break;
                case 'innerborder':
                    list($innerborderwidth, $innerbordercolor) = explode(',', $v);
                    break;

                case 'outerborderwidth':
                    $outerborderwidth = $v;
                    break;
                case 'outerbordercolor':
                    $outerbordercolor = $v;
                    break;
                case 'outerborder':
                    list($outerborderwidth, $outerbordercolor) = explode(',', $v);
                    break;

                case 'resizeleft':
                    $resizeleft = $v;
                    break;
                case 'resizetop':
                    $resizetop = $v;
                    break;
                case 'resizemode':

                    switch($v)
                    {
                        case 'KEEP_ASPECT_RATIO':
                            $resizemode = Func_Thumbnailer::KEEP_ASPECT_RATIO;
                            break;
                        case 'CROP_CENTER':
                            $resizemode = Func_Thumbnailer::CROP_CENTER;
                            break;
                    }

                    break;

                case 'default':
                    $default = $v;
                    break;

                case 'saveas':
                    $saveas = $v;
                    break;

                case 'id_user':
                case 'id_directory_entry':
                case 'id_article':
                case 'path':
                case 'src':     // OFImg
                case 'ovsrc':   // OFImg
                case 'id_file':
                    $sourceMethod = $p;
                    $sourceValue = $v;
                    break;

                default:
                    $this->externalAttributes[$p] = $v;
                    break;

            }
        }



        // get image URL by method


        $sourceset = false;
        if ('' !== $sourceValue) {
            $sourceset = $this->setThumbnailerSource($T, $sourceMethod, $sourceValue);
        }

        if (!$sourceset && null !== $default) {
            $T->setSourceFile($default);
            $sourceset = true;
        }


        if ($sourceset)
        {
            $T->setResizeMode($resizemode, $resizeleft, $resizetop);
            $T->setBorder($outerborderwidth, $outerbordercolor, $innerborderwidth, $innerbordercolor);

            $imageUrl = $T->getThumbnail($maxwidth, $maxheight);
            $this->computedWidth = $T->getComputedWidth();
            $this->computedHeight = $T->getComputedHeight();
        }

        if ($saveas) {
            $this->saveAs($saveas, $imageUrl, $imageUrl);
            $imageUrl = null;
        }

        return $this->renderOutput($imageUrl, $sourceMethod, $sourceValue);
    }


    /**
     * Set thumbnailer source image
     * return true if source is set
     * @return bool
     */
    private function setThumbnailerSource($T, $sourceMethod, $sourceValue)
    {

        switch ($sourceMethod) {

            case 'id_user':
                return $this->setDirEntrySource($T, bab_getDirEntry($sourceValue, BAB_DIR_ENTRY_ID_USER));

            case 'id_directory_entry':
                return $this->setDirEntrySource($T, bab_admGetDirEntry($sourceValue, BAB_DIR_ENTRY_ID));

            case 'id_article':
                return $this->setArticleSource($T, $sourceValue);

            case 'path':
                $T->setSourceFile($sourceValue);
                return true;

            case 'id_file':
                include_once $GLOBALS['babInstallPath'].'utilit/fileincl.php';
                $access = fm_getFileAccess($sourceValue);

                if (!$access['bdownload'])
                {
                    return false;
                }
                $T->setSourceFile($access['oFolderFile']->getFullPathname());
                return true;
        }

        return false;
    }


    /**
     * Render output of the ovml function
     * @param string $imageUrl         set to null if saveas is active
     * @param string $sourceMethod
     * @param string $sourceValue
     *
     */
    protected function renderOutput($imageUrl, $sourceMethod, $sourceValue)
    {
        return (string) $imageUrl;
    }



    /**
     * Save output value to variable
     * @param string $variable     OVML variable name
     * @param string $imageUrl     Thumbnail image URL
     * @param string $value        Value to save in variable
     */
    protected function saveAs($variable, $imageUrl, $value)
    {
        if (!isset($variable)) {
            return;
        }

        if (!isset($imageUrl)) {
            $this->gctx->push($variable, '');
            return;
        }

        $this->gctx->push($variable, $value);
    }



    /**
     * Set image source from directory entry array
     *
     * @param Func_Thumbnailer $T
     * @param Array $entry
     *
     * return bool
     */
    private function setDirEntrySource(Func_Thumbnailer $T, $entry)
    {
        if (!$entry)
        {
            bab_debug('OFThumbnail : directory entry does not exists');
            return false;
        }

        if (!isset($entry['jpegphoto']['photo']))
        {
            return false;
        }

        $photo = $entry['jpegphoto']['photo'];
        $data = $photo->getData();
        if (null !== $data && '' !== $data)
        {
            $T->setSourceBinary($data, $photo->lastUpdate());
            return true;
        }

        return false;
    }


    /**
     * Set image source from article image
     *
     * @param Func_Thumbnailer $T
     * @param int $id_article
     * @return boolean
     */
    private function setArticleSource(Func_Thumbnailer $T, $id_article)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';
        require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';

        $arr = bab_getImageArticle($id_article);

        if (!$arr)
        {
            // no photo
            return false;
        }

        $settings = bab_getInstance('bab_Settings');
        /*@var $settings bab_Settings */
        $site = $settings->getSiteSettings();

        $imagePath = $site['uploadpath'].'/'.$arr['relativePath'].$arr['name'];

        if (!file_exists($imagePath))
        {
            bab_debug(sprintf('Image not found for article %d with path : %s', $id_article, $imagePath));
            return false;
        }

        $T->setSourceFile($imagePath);
        return true;
    }
}
