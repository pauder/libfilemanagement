<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/xmlinarchiveincl.php';
require_once dirname(__FILE__).'/metadata.class.php';



class lfm_OdtMetadataParser extends lfm_XmlInArchiveMetadataParser {

	

	/**
	 * Parse metadata
	 */
	public function parse() {

		if ($this->parsed) {
			return true;
		}

		if (!$this->isValid()) {
			return false;
		}

		$filepath = $this->filepath;

		$reader = new XMLReader();

		if (!@$reader->open('zip://' . $filepath . '#meta.xml')) {
			return false;
		}

		$this->meta = array();
		while ($reader->read()) {
			if ($reader->nodeType == XMLREADER::ELEMENT) {
				$elm = $reader->name;
			} else {
				if ($reader->nodeType == XMLREADER::END_ELEMENT && $reader->name == 'office:meta') {
					break;
				}
				if (!trim($reader->value)) {
					continue;
				}

				$value = bab_getStringAccordingToDataBase($reader->value, 'UTF-8');
				if (isset($this->meta[$elm])) {
					$this->meta[$elm] .= ', '.$value;
				} else {
					$this->meta[$elm] = $value;
				}
			}
			
			if ('meta:document-statistic' === $reader->name) {
				$this->meta['meta:page-count'] = (int) $reader->getAttribute('meta:page-count');
				$this->meta['meta:word-count'] = (int) $reader->getAttribute('meta:word-count');
			}
		}

		$this->parsed = true;
	}
}



class lfm_OdtDocMetadata extends lfm_MetadataNamespace_Doc {

	private $parsed = false;


	public function getAllMeta() {

		$parser = bab_getInstance('lfm_OdtMetadataParser');

		if ($parser->isValid()) {

			return array(
				'Title',
				'Subject',
				'Keywords',
				'Comments',
				'Author',
				'PageCount',
				'WordCount',
				'Created'
			);

		}

		return array();
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {
		if (false === $this->parsed) {
			$this->parsed = true;
			$parser = bab_getInstance('lfm_OdtMetadataParser');
			$parser->setFilePath($this->getFilePath());
			$parser->parse();

			$odtinfo = $parser->meta;

			$Title 		= isset($odtinfo['dc:title']) 				? $odtinfo['dc:title'] : '';
			$Subject	= isset($odtinfo['dc:subject']) 			? $odtinfo['dc:subject'] : '';
			$Keywords	= isset($odtinfo['meta:keyword']) 			? $odtinfo['meta:keyword'] : '';
			$Comments	= isset($odtinfo['dc:description']) 		? $odtinfo['dc:description'] : '';
			$Author		= isset($odtinfo['dc:creator']) 			? $odtinfo['dc:creator'] : '';
			$PageCount	= isset($odtinfo['meta:page-count']) 		? $odtinfo['meta:page-count'] : '';
			$WordCount	= isset($odtinfo['meta:word-count']) 		? $odtinfo['meta:word-count'] : '';
			$Created	= isset($odtinfo['meta:creation-date']) 	? $odtinfo['meta:creation-date'] : '';
			
			// 2010-06-16T17:18:20.25
			
			if ($Created)
			{
				list($date,$time)			= explode('T', $Created);
				list($year, $month, $day)	= explode('-', $date);
				list($hour,$min, $seconds)	= explode(':', $time);
				$Created					= mktime($hour, $min, round($seconds), $month, $day, $year);
			}
			

			$this->setMeta('Title'		, $Title);
			$this->setMeta('Subject'	, $Subject);
			$this->setMeta('Keywords'	, $Keywords);
			$this->setMeta('Comments'	, $Comments);
			$this->setMeta('Author'		, $Author);
			$this->setMeta('PageCount'	, $PageCount);
			$this->setMeta('WordCount'	, $WordCount);
			$this->setMeta('Created'	, $Created);
		}

		return $this->getStoredValue($name);
	}
	
	
	
	public function getDisplayValue($name) 
	{
		switch($name)
		{

			case 'Created':
				return bab_shortDate($this->getMetaValue($name));
			
		}
		
		return $this->getMetaValue($name); 
	}

}
















class lfm_OdtFileMetadata extends lfm_MetadataNamespace_File {

	private $parsed = false;


	public function getAllMeta() {

		$parser = bab_getInstance('lfm_OdtMetadataParser');

		if ($parser->isValid()) {

			return array(
				'Created',
				'Modified'
			);

		}

		return array();
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {
		if (false === $this->parsed) {
			$this->parsed = true;
			$parser = bab_getInstance('lfm_OdtMetadataParser');
			$parser->setFilePath($this->getFilePath());
			$parser->parse();

			$odtinfo = $parser->meta;

			$Created	= isset($odtinfo['meta:creation-date']) 	? $odtinfo['meta:creation-date'] : '';
			$Modified	= isset($odtinfo['dc:date']) 				? $odtinfo['dc:date'] : '';

			if ($Created)
			{
				list($date,$time)			= explode('T', $Created);
				list($year, $month, $day)	= explode('-', $date);
				list($hour,$min, $seconds)	= explode(':', $time);
				$Created					= mktime($hour, $min, round($seconds), $month, $day, $year);
			}
			
			
			if ($Modified)
			{
				list($date,$time)			= explode('T', $Modified);
				list($year, $month, $day)	= explode('-', $date);
				list($hour,$min, $seconds)	= explode(':', $time);
				$Modified					= mktime($hour, $min, round($seconds), $month, $day, $year);
			}
			
			$this->setMeta('Created'	, $Created);
			$this->setMeta('Modified'	, $Modified);
		}

		return $this->getStoredValue($name);
	}
	
	
	
	public function getDisplayValue($name) 
	{
		$value = $this->getMetaValue($name);
		if (null === $value)
		{
			return null;
		}
		
		
		
		switch($name)
		{
			case 'Created':
			case 'Modified':
				return bab_shortDate($value);
			
		}
		
		return $this->getMetaValue($name); 
	}

}

