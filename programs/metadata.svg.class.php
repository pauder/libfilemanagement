<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/metadata.class.php';




class lfm_SvgMetadata extends lfm_MetadataNamespace_Image {

	private $parsed = false;
	private $meta	= array();


	public function getAllMeta() {

		$arr = array(
			'Height',
			'Width',
			'Title',
			'Description',
			'Keywords'
		);

		return $arr;
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {

		if (false === $this->parsed) {
	
			$xml = simplexml_load_file($this->getFilePath());
			$attributes = $xml->attributes();
			

			$Height = isset($attributes->height) ? (int) $attributes->height : '';
			$Width = isset($attributes->width) 	 ? (int) $attributes->width : '';

			$xml->registerXPathNamespace('dc', 'http://purl.org/dc/elements/1.1/');
			$xml->registerXPathNamespace('cc', 'http://creativecommons.org/ns#');
			$xml->registerXPathNamespace('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#');

			/*
			$result = $xml->xpath('//dc:creator/cc:Agent/dc:title');
			$Author = $result ? reset($result) : '';
			*/

			$result = $xml->xpath('//cc:Work/dc:title');
			$Title = $result ? reset($result) : '';

			$result = $xml->xpath('//cc:Work/dc:description');
			$Description = $result ? reset($result) : '';


			$arr = array();
			foreach($xml->xpath('//dc:subject/rdf:Bag/rdf:li') as $result) {
				$arr[] = $result ? reset($result) : '';
			}
			$Keywords = implode(', ', $arr);

			$this->setMeta('Height'			, $Height);
			$this->setMeta('Width'			, $Width);
			$this->setMeta('Title'			, $Title);
			$this->setMeta('Description'	, $Description);
			$this->setMeta('Keywords'		, $Keywords);

		}

		return $this->getStoredValue($name);
	}


	



}





