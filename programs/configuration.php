<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once dirname(__FILE__) . '/functions.php';

function lfm_configurationMenu($message = null)
{
    global $babBody;

    /* @var $Icons Func_Icons */
    $Icons = bab_Functionality::get('Icons');
    $Icons->includeCss();

    $babBody->setTitle('LibFileManagement configuration');

    if (isset($message)) {
        $babBody->addError($message);
    }

    $thumbnailPath = new bab_Path(lfm_getThumbnailBasePath());

    $url = bab_url::request('tg');
    $url = bab_url::mod($url, 'idx', 'clearThumbnails');




    $html = '
        <div class="BabLoginCadreBackground" style="width: 90%; margin: auto;">
        <div class="BabLoginMenuBackground">

        <h2>' . lfm_translate('Thumbnails') . '</h2>
        <p>' . sprintf(lfm_translate('There are currently %d thumbnail files in the server cache.'), $thumbnailPath->count()) . '</p>

        <div class="icon-left-16 icon-left icon-16x16">
        <form method="POST">';
    if (class_exists('bab_CsrfProtect')) {
        $csrf = bab_getInstance('bab_CsrfProtect');
        $html.= '<input type="hidden" name="babCsrfProtect" value="' . bab_toHtml($csrf->getToken()) . '" />';
    }

    $html.= '<input type="hidden" name="tg" value="' . bab_rp('tg') . '" />
        <input type="hidden" name="idx" value="clearThumbnails" />
        <input type="submit" class="icon ' . Func_Icons::ACTIONS_EDIT_DELETE . '" value="' . lfm_translate('Clear thumbnails') . '" />
        </form>
        </div>
        </div>
        </div>';

    $babBody->babEcho($html);


}



/**
* Delete a folder content recursively
* return true on success
* @param	string	$dir		folder to delete
* @param	string	&$msgerror	this string will be empty on succes and not empty on failure
* @return	bool
*/
function lfm_deldircontent($dir, &$msgerror) {

    if (!file_exists($dir))
    {
        $msgerror = bab_sprintf(bab_translate('The folder does not exists : %s'), $dir);
        return false;
    }


    $current_dir = opendir($dir);
    while($entryname = readdir($current_dir)){
        if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!="..")){
            if (false === bab_deldir($dir.'/'.$entryname, $msgerror)) {
                return false;
            }
        } elseif ($entryname != "." and $entryname!="..") {
            if (false === unlink($dir.'/'.$entryname)) {
                $msgerror = bab_sprintf(bab_translate('The file is not deletable : %s'), $dir.'/'.$entryname);
                return false;
            }
        }
    }
    closedir($current_dir);
    return true;
}



require_once dirname(__FILE__) . '/thumbnail.php';

$idx = bab_rp('idx');

switch($idx) {

    case 'clearThumbnails':
        $thumbnailPath = new bab_Path(lfm_getThumbnailBasePath());

        $error = '';
        if (!lfm_deldircontent($thumbnailPath->toString(), $error)) {
            lfm_configurationMenu($error);
            break;
        }
        lfm_configurationMenu(lfm_translate('All thumbnails have been deleted.'));

        break;

    default:
    case 'menu':
        lfm_configurationMenu();
        break;
}
