<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

/** 
 * @return lfm_MetaNamespaces
 */
function lfm_getHtmlMetadata($filepath) {

	require_once dirname(__FILE__).'/metadata.html.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('Doc', 'lfm_HtmlMetadata');

	return $ns;
}



/** 
 * @return lfm_MetaNamespaces
 */
function lfm_getPdfMetadata($filepath) {

	require_once dirname(__FILE__).'/metadata.pdf.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('Doc', 'lfm_PdfMetadata');

	return $ns;

}



/** 
 * @return lfm_MetaNamespaces
 */
function lfm_getOdtMetadata($filepath) {


	require_once dirname(__FILE__).'/metadata.odt.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('File', 'lfm_OdtFileMetadata');
	$ns->addNamespace('Doc', 'lfm_OdtDocMetadata');
	

	return $ns;

}




function lfm_getOpenXmlMetadata($filepath) {
	require_once dirname(__FILE__).'/metadata.openxml.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('File', 'lfm_OpenXmlFileMetadata');
	$ns->addNamespace('Doc', 'lfm_OpenXmlDocMetadata');
	

	return $ns;

}






/** 
 * @return lfm_MetaNamespaces
 */
function lfm_getJpegMetadata($filepath) {

	require_once dirname(__FILE__).'/metadata.exif.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('Image', 'lfm_ExifMetadata');
	
	return $ns;
}






/** 
 * @return lfm_MetaNamespaces
 */
function lfm_getMp3Metadata($filepath) {

	require_once dirname(__FILE__).'/metadata.id3.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('Audio', 'lfm_Id3Metadata');
	
	return $ns;
}






function lfm_getVideoMetadata($filepath) {

	require_once dirname(__FILE__).'/metadata.video.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('Doc', 'lfm_VideoDocMetadata');
	$ns->addNamespace('Audio', 'lfm_VideoAudioMetadata');
	
	return $ns;
}





function lfm_getZipMetadata($filepath) {

	require_once dirname(__FILE__).'/metadata.zip.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);
	$ns->addNamespace('Doc', 'lfm_ZipMetadata');
	return $ns;
}




function lfm_getSvgMetadata($filepath) {	

	require_once dirname(__FILE__).'/metadata.svg.class.php';

	$ns = new lfm_MetaNamespaces;
	$ns->setFilePath($filepath);

	$ns->addNamespace('Image', 'lfm_SvgMetadata');
	
	return $ns;
}








/**
 * @return lfm_MetaNamespaces 	or false if file is not readable
 */
function lfm_getMetadata($filepath, $mime) {
	

	if (!is_readable($filepath)) {
		return false;
	}

	switch($mime) {
	
		case 'text/html':
			return lfm_getHtmlMetadata($filepath);
			
		case 'application/pdf':
			return lfm_getPdfMetadata($filepath);
			
		case 'application/vnd.oasis.opendocument.text':
			return lfm_getOdtMetadata($filepath);

		case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			return lfm_getOpenXmlMetadata($filepath);
			
		case 'image/jpeg':
			return lfm_getJpegMetadata($filepath);
			
		case 'audio/mpeg':
			return lfm_getMp3Metadata($filepath);

		case 'video/mp4':
		case 'video/mpeg':
		case 'video/quicktime':
		case 'video/x-flv':
		case 'video/x-ms-wmv':
		case 'video/x-msvideo':
			return lfm_getVideoMetadata($filepath);

		case 'application/zip':
			return lfm_getZipMetadata($filepath);

		case 'image/svg+xml':
			return lfm_getSvgMetadata($filepath);
	
		default:
			require_once dirname(__FILE__).'/metadata.class.php';
			return new lfm_MetaNamespaces;
	}
}