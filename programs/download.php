<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
 

require_once dirname(__FILE__).'/functions.php';


function getfile()
{
	$tocken = bab_rp('tocken');
	
	
	if (!$tocken || !isset($_SESSION['LibFileManagement']['DownloadUrl'][$tocken]))
	{
		die();
	}
	
	
	$uid = $_SESSION['LibFileManagement']['DownloadUrl'][$tocken];
	
	
	if (!$uid)
	{
		die();
	}
	
	
	$files = lfm_getFiles($uid);
	
	if (1 === count($files)) {
		$filename = reset($files);
	} else {
	
		foreach($files as $file) {
			if (bab_rp('filename') === basename($file)) {
				$filename = $file;
			}
		}
	}
	
	if (!isset($filename)) {
		global $babBody;
		$babBody->addError(lfm_translate('The file does not exists'));
		$babBody->babPopup('');
	}

	$disposition = isset($_GET['inline']) ? 'inline' : 'attachment';

	if( strtolower(bab_browserAgent()) == "msie"){
		header('Cache-Control: public');
	}
	header("Content-Disposition: ".$disposition."; filename=\"".basename($filename)."\"\n");
	header("Content-Type: ".bab_getFileMimeType($filename)."\n");
	header("Content-Length: ". filesize($filename)."\n");
	header("Content-transfert-encoding: binary\n");
	
	if ($fp=fopen($filename,"rb")) {
		while (!feof($fp)) {
			print fread($fp, 8192);
			}
		fclose($fp);
	}
	
	die();
}


getfile();
