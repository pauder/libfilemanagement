<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/metadata.class.php';




class lfm_PdfMetadata extends lfm_MetadataNamespace_Doc {

	private $parsed = false;


	public function pdfinfo_version() {
		static $version = NULL;
		if (NULL === $version) {
			$version = 0;
			if ($output = lfm_execCmd('pdfinfo -v 2>&1')) {
				if (preg_match('/pdfinfo\s+version\s+([\d\.]+)/', $output, $matches)) {
					$version = $matches[1];
				}
			}
		}
		return $version;
	}


	public function getAllMeta() {

		$version = $this->pdfinfo_version();
		

		if (version_compare('0.1', $version, '<=')) {

			return array(
				'Title',
				'Subject',
				'Author',
				'Keywords',
				'PageCount'
			);

		} else {
			return array();
		}
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {
		if (false === $this->parsed) {
			$this->parsed = true;
			$pdfinfo = $this->pdfinfo();
			
			$Title 		= isset($pdfinfo['Title']) 		? $pdfinfo['Title'] : '';
			$Subject	= isset($pdfinfo['Subject']) 	? $pdfinfo['Subject'] : '';
			$Author		= isset($pdfinfo['Author']) 	? $pdfinfo['Author'] : '';
			$Keywords	= isset($pdfinfo['Keywords']) 	? $pdfinfo['Keywords'] : '';
			$Pages		= isset($pdfinfo['Pages']) 		? $pdfinfo['Pages'] : '';

			$this->setMeta('Title'		, $Title);
			$this->setMeta('Subject'	, $Subject);
			$this->setMeta('Author'		, $Author);
			$this->setMeta('Keywords'	, $Keywords);
			$this->setMeta('PageCount'	, $Pages);
		}

		return $this->getStoredValue($name);
	}


	


	
	/**
	 * Get pdf metadata informations
	 * @return	array
	 */
	function pdfinfo() {

		$pdffile = $this->getFilePath();

		$return = array();
		$output = shell_exec("pdfinfo \"$pdffile\"  2>&1");
		
		if (empty($output)) {
			return false;
		}

		$output = lfm_getStringFromCommandLine($output);
		
		$lines = preg_split('/\n|\r\n|\r/', $output);
		foreach($lines as $line) {
			$key = trim(substr($line, 0, 16),' :');
			$value = trim(substr($line, 16));
			if ($key && $value) {
				$return[$key] = $value;
			}
		}

		return $return;
	}
}





