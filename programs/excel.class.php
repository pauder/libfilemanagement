<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/functions.php';



/**
 * Excel export functionality
 */
class Func_ExcelExport extends bab_functionality {

	private $recordMethod = null;
	private $filename	= null;


	/**
	 * Set correct headers to download as excel file
	 * @param	string	$filename
	 */
	private function sendHeaders() {

      	header("Pragma: public");
      	header("Expires: 0");
      	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
     	header("Content-Type: application/force-download");
      	header("Content-Type: application/vnd.ms-excel");
      	header('Content-Disposition: attachment;filename="'.$this->filename.'"');
      	header("Content-Transfer-Encoding: binary ");
	}

	/**
	 * Get functionality description
	 */
	public function getDescription() {
   		return lfm_translate('Export to excel file');
	}
	
	/**
	 * Set the output method to download, the excel file will be sent to stdout
	 * and set the filename used for the download
	 * @param	string	$filename	
	 * @return Func_ExcelExport
	 */
	public function setDownloadFilename($filename) {
	
		$filename = str_replace(array('/','\\', ';', "'", '"'), array('-', '-', '', '', ''), $filename);
		$this->filename = $filename;
		$this->recordMethod = 'download';
		return $this;
	}
	
	/**
	 * set the output method to file, the excel workbook will be writed in the file on disk
	 * @param	string	$filename	full path name to a file on disk
	 * @return Func_ExcelExport
	 */
	public function setRecordFilename($filename) {
		$this->filename = $filename;
		$this->recordMethod = 'file';
		return $this;
	}	

	
	
	/**
	 * Get the workbook to work with
	 * 
	 * @param	string		$version		0 		old version
	 * 										0.9.2	new version with the Spreadsheet_Excel_Writer_Workbook class
	 * 
	 * 
	 * 
	 * @return Workbook
	 */
	public function getWorkbook($version = '0') {

		switch($version)
		{
			case '0':
				if (!class_exists('Worksheet')) {
					require_once dirname(__FILE__).'/excel/Worksheet.php';
					require_once dirname(__FILE__).'/excel/Workbook.php';
				}
				
				$f = $this->getFileNameAndInitHeaders();
				return new Workbook($f);
				
			case '0.9.2':
				if (!class_exists('Spreadsheet_Excel_Writer_Workbook')) {
					require_once dirname(__FILE__).'/excel/Spreadsheet_Excel_Writer/Writer/Workbook.php';
				}
				
				$f = $this->getFileNameAndInitHeaders();
				return new Spreadsheet_Excel_Writer_Workbook($f);
				
		}
	}
	
	
	
	private function getFileNameAndInitHeaders()
	{
		switch($this->recordMethod) {
			
			case 'download':
				$this->sendHeaders();
				return '-';
				break;
				
			case 'file':
				return $this->filename;
				break;
		}
	}
	
	
	/**
	 * Convert a numeric column number to excel column title letter (0 => A ; 1 => B...)
	 * @param	int		$column
	 * @return string
	 */
	public function getColumnLetter($column) {
		$cols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; /* 26 characters */
		
		if ($column > (strlen($cols)-1)) {
			/* number greater than 25 (than Z) */
			$firstchar 	= floor($column / strlen($cols)) - 1;
			$secondchar = $column - strlen($cols);
			
			return $cols{$firstchar}.$cols{$secondchar};
		} else {
			/* number less than or equal 25 (than Z) */
			return $cols{$column};
		}
	}
	
	
	/**
	 * Get excel date
	 * number of seconds since excel 0 day (1899-12-30)
	 * 
	 * @param	int		$time		Unix timestamp
	 * 
	 * @return int
	 */
	public function getExcelDate($time)
	{
		if (0 >= $time)
		{
			return null;
		}
		
		$time = gmmktime(date('H', $time), date('i', $time), date('s', $time), date('m', $time), date('d', $time), date('Y', $time));

		return 25569 + ($time / 86400);
	}
	
	
	
	/**
	 * get instance of spreadsheet excel reader to read xls file
	 * use ->read() to read a xls file
	 * use ->sheets for data
	 * 
	 * 
	 * @param string	$version	Base version of products
	 * 								'sf'	default version is from http://sourceforge.net/projects/phpexcelreader
	 * 								'gg'	current version 2.21 from http://code.google.com/p/php-excel-reader/
	 * 
	 * @return Spreadsheet_Excel_Reader
	 */
	public function getReader($version = 'gg')
	{	
		if ('gg' === $version)
		{
			require_once dirname(__FILE__).'/excel/excel_reader2.php';
			
		} elseif ('sf' === $version) {
			
			require_once dirname(__FILE__).'/excel/reader.php';
		}
		
		$reader = new Spreadsheet_Excel_Reader();
		if (!function_exists('iconv'))
		{
			$reader->setUTFEncoder('mb');
		}
		$reader->setOutputEncoding(bab_charset::getIso());
		
		return $reader;
	}
}