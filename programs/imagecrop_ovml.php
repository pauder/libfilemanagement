<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

bab_functionality::includeFile('Ovml/Function');



/**
 * Create a thumbnail image
 *
 * <OFImageCrop
 * 		( id_directory_entry="" | id_user="" | id_article="" | path="absolute/path/to/image" )
 * 		[rotate="0"]
 * 		x=""
 * 		y=""
 * 		width=""
 * 		height=""
 * 		maxwidth=""
 * 		maxheight=""
 * 		[saveas=""]
 * >
 *
 */
class Func_Ovml_Function_ImageCrop extends Func_Ovml_Function
{

    /**
     * List of attribute not managed by the ovml function
     * used by OFImg
     *
     * @var array
     */
    protected $externalAttributes = array();


    public function toString()
    {
        $T = bab_functionality::get('Thumbnailer');
        /*@var $T Func_Thumbnailer */


        if (!$T
             || (!isset($this->args['id_directory_entry']) && !isset($this->args['id_user']) && !isset($this->args['id_article']) && !isset($this->args['path']))
             || !isset($this->args['x'])
             || !isset($this->args['y'])
             || !isset($this->args['width'])
             || !isset($this->args['height'])
             || !isset($this->args['maxwidth'])
             || !isset($this->args['maxheight'])
        ) {
            return '';
        }

        $rotate = 0;
        $x = null;
        $y = null;
        $width = null;
        $height = null;
        $maxwidth = null;
        $maxheight = null;
        $saveas = null;
        $imageUrl = null;
        $sourceMethod = null;
        $sourceValue = null;

        foreach ($this->args as $p => $v) {

            $p = mb_strtolower(trim($p));

            switch ($p) {

                case 'x':
                case 'y':
                case 'rotate':
                    if ($v < 0) {
                        trigger_error(sprintf('Invalid parameter '.$p.'=%s in OFImageCrop', $v));
                        break;
                    }
                    $$p = $v;
                    break;

                case 'width':
                case 'height':
                case 'maxwidth':
                case 'maxheight':
                    if ($v <= 0) {
                        trigger_error(sprintf('Invalid parameter '.$p.'=%s in OFImageCrop', $v));
                        break;
                    }
                    $$p = $v;
                    break;

                case 'saveas':
                    $saveas = $v;
                    break;

                case 'id_user':
                case 'id_directory_entry':
                case 'id_article':
                case 'path':
                case 'src':     // OFImg
                case 'ovsrc':   // OFImg
                    $sourceMethod = $p;
                    $sourceValue = $v;
                    break;

                default:
                    $this->externalAttributes[$p] = $v;
                    break;

            }
        }



        // get image URL by method
        $sourceset = false;
        if ('' !== $sourceValue) {
            $sourceset = $this->setThumbnailerSource($T, $sourceMethod, $sourceValue);
        }

        $T->setResizeMode(Func_Thumbnailer::CROP_CENTER);

        if ($sourceset) {
            $T->setCropArea($x, $y, $width, $height, $rotate);
            $imageUrl = $T->getThumbnail($maxwidth, $maxheight);
        } else {
            return '';
        }

        if ($saveas) {
            $this->saveAs($saveas, $imageUrl, $imageUrl);
            $imageUrl = null;
        }

        return $this->renderOutput($imageUrl, $sourceMethod, $sourceValue);
    }


    /**
     * Set thumbnailer source image
     * return true if source is set
     * @return bool
     */
    private function setThumbnailerSource($T, $sourceMethod, $sourceValue)
    {

        switch ($sourceMethod) {

            case 'id_user':
                return $this->setDirEntrySource($T, bab_getDirEntry($sourceValue, BAB_DIR_ENTRY_ID_USER));

            case 'id_directory_entry':
                return $this->setDirEntrySource($T, bab_getDirEntry($sourceValue, BAB_DIR_ENTRY_ID));

            case 'id_article':
                return $this->setArticleSource($T, $sourceValue);

            case 'path':
                $T->setSourceFile($sourceValue);
                return true;
        }

        return false;
    }


    /**
     * Render output of the ovml function
     * @param string $imageUrl         set to null if saveas is active
     * @param string $sourceMethod
     * @param string $sourceValue
     *
     */
    protected function renderOutput($imageUrl, $sourceMethod, $sourceValue)
    {
        return (string) $imageUrl;
    }



    /**
     * Save output value to variable
     * @param string $variable     OVML variable name
     * @param string $imageUrl     Thumbnail image URL
     * @param string $value        Value to save in variable
     */
    protected function saveAs($variable, $imageUrl, $value)
    {
        if (!isset($variable)) {
            return;
        }

        if (!isset($imageUrl)) {
            $this->gctx->push($variable, '');
            return;
        }

        $this->gctx->push($variable, $value);
    }



    /**
     * Set image source from directory entry array
     *
     * @param Func_Thumbnailer $T
     * @param Array $entry
     *
     * return bool
     */
    private function setDirEntrySource(Func_Thumbnailer $T, $entry)
    {
        if (!$entry)
        {
            bab_debug('OFImageCrop : directory entry does not exists');
            return false;
        }

        if (!isset($entry['jpegphoto']['photo']))
        {
            return false;
        }

        $photo = $entry['jpegphoto']['photo'];
        $data = $photo->getData();
        if (null !== $data && '' !== $data)
        {
            $T->setSourceBinary($data, $photo->lastUpdate());
            return true;
        }

        return false;
    }


    /**
     * Set image source from article image
     *
     * @param Func_Thumbnailer $T
     * @param int $id_article
     * @return boolean
     */
    private function setArticleSource(Func_Thumbnailer $T, $id_article)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';
        require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';

        $arr = bab_getImageArticle($id_article);

        if (!$arr)
        {
            // no photo
            return false;
        }

        $settings = bab_getInstance('bab_Settings');
        /*@var $settings bab_Settings */
        $site = $settings->getSiteSettings();

        $imagePath = $site['uploadpath'].'/'.$arr['relativePath'].$arr['name'];

        if (!file_exists($imagePath))
        {
            bab_debug(sprintf('Image not found for article %d with path : %s', $id_article, $imagePath));
            return false;
        }

        $T->setSourceFile($imagePath);
        return true;
    }
}
