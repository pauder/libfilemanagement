<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

// test metadata



function lfm_unittests() {

	global $babBody;

	$filepathlist = array();


	$filepathlist[] = '/home/paul/Documents/projets/cub/D09EC.pdf'; 
	$filepathlist[] = '/home/paul/Documents/download/fichier.html';
	$filepathlist[] = '/home/paul/Documents/projets/idlogistics/20091501CR01.odt';
	$filepathlist[] = '/home/paul/Documents/Ma musique/Raintime.mp3';
	$filepathlist[] = '/home/paul/Musique/Jazz Liberatorz/Clin d\'oeil/03 - I am hip hop feat. Asheru.mp3';
	$filepathlist[] = '/home/paul/Documents/media/images/20080605_Ruin_in_Theologos.jpg';
	$filepathlist[] = '/home/paul/Documents/media/images/Henri_Matisse.jpg';
	$filepathlist[] = '/home/paul/Documents/media/images/IMG_3211.JPG';
	$filepathlist[] = '/home/paul/Documents/media/images/IMG_3217.JPG';
	$filepathlist[] = '/home/paul/Documents/Mes vidéos/Floridafs.wmv';
	$filepathlist[] = '/home/paul/Documents/Mes vidéos/Robin Hood 207 Donne-Moi Ta Main.avi';
	$filepathlist[] = '/home/paul/Documents/download/Convention.docx';
	$filepathlist[] = '/home/paul/Documents/projets/bva/demdevis.flv';
	$filepathlist[] = '/home/paul/Documents/download/Formulaire_de_remboursement.xlsx';
	$filepathlist[] = '/home/paul/Documents/download/Bureau.zip';
	$filepathlist[] = '/home/paul/Documents/download/paul.zip';
	$filepathlist[] = '/home/paul/Images/logos/Cantico.svg';
	$filepathlist[] = '/home/paul/Images/axiw_logo.png';

	$fileinfo = bab_functionality::get('FileInfos');


	/*
	$mime1 = $func->getMimeTypeFromFile($filepath);
	$mime2 = $func->getMimeTypeFromExtension($filepath);
	$type1 = $func->getFileTypeFromMimeType($mime1);
	$type2 = $func->getFileTypeFromMimeType($mime2);
	$generic = $func->getGenericClassName($filepath);
	
	bab_debug("filepath : $filepath \nfile mime : $mime1 \nextension mime : $mime2 \nfile type : $type1 \nextension type : $type2 \ngeneric : $generic");
	

	*/


	$thumb = bab_functionality::get('Thumbnailer');
	// $thumb->setBackgroundColor();


	

	foreach($filepathlist as $filepath) {
		$m = $fileinfo->getMetadata($filepath);


		$str = '<table border="1" width="100%">';
		$str .= sprintf('<caption>%s</caption>', bab_toHtml($filepath));

		if ($m) {

			foreach($m->getAllNs() as $ns) {

				$meta = $m->$ns;
				foreach($meta->getAllMeta() as $name) {

					$str .= sprintf('<tr><td width="200">%s</td><td>%s</td><td width="50%%">%s</td></tr>',
						bab_toHtml($ns.'.'.$name), 
						bab_toHtml($meta->getTitle($name)), 
						bab_toHtml((string) $meta->$name)
					);
				}
			}
		}

		$str .= '</table>';


		$image = bab_toHtml($thumb->getIcon($filepath, 128, 128));
		


		$html = sprintf('<table width="100%%" style="background:lightblue">
				<tr>
					<td width="128"><img src="%s" alt="" /></td>
					<td>%s</td>
				</tr>
			</table><br /><br /><br />', $image, $str);

		$babBody->babEcho($html);
	}



	

	/*
	$entry = bab_getDirEntry();
	if (isset($entry['jpegphoto']['photo'])) {
		$photo = $entry['jpegphoto']['photo'];

		$thumb->setSourceBinary(
			$photo->getData(),
			$photo->lastUpdate()
		);

		$thumb->setBorder(1 , '#aaa', 10, '#eee');
		
		$url = $thumb->getThumbnailOrDefault(150,200);
		$babBody->babEcho(sprintf('<p align="center"><img src="%s" alt="%s" /></p>', $url, $GLOBALS['BAB_SESS_USER']));
	}
	*/

}



lfm_unittests();