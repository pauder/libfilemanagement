<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/metadata.class.php';



class lfm_ZipMetadata extends lfm_MetadataNamespace_Doc {

	
	public function getAllMeta() {
		if (!class_exists('ZipArchive')) {
			return array();
		}


		return array('Comments');
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return 	mixed
	 */
	public function getMetaValue($name) {

		if ('Comments' === $name && class_exists('ZipArchive')) {
			$zip = new ZipArchive;

			if ($zip->open($this->getFilePath())) {
				return bab_getStringAccordingToDatabase($zip->getArchiveComment(), 'ISO-8859-15');
			}
		}

		return null;
	}


}




