<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * This file provides compatibility classes: lfm_Ffmpeg_Frame and lfm_Ffmpeg_Movie which
 * should be almost compatible with ffmpeg_movie and ffmpeg_frame (defined in the ffmpeg php extension).
 * Those compatibility classes use the command line ffmpeg tool.
 * 
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


if (class_exists('ffmpeg_movie')) {

	class lfm_Ffmpeg_Movie extends ffmpeg_movie { }

} else {

	/**
	 * A class compatible with ffmpeg_frame, using the command line ffmpeg tool.
	 */
	class lfm_Ffmpeg_Frame
	{
		private $image = null;

		/**
		 * Create a frame object from a GD image.
		 * 
		 * @param resource $image		A gd image.
		 */
		public function __construct($image)
		{
			$this->image = $image;
		}

		/**
		 * Return the width of the frame.
		 * 
		 * @return int
		 */
		public function getWidth()
		{
			return imagesx($this->image);
		}
		
		/**
		 * Return the height of the frame.
		 * 
		 * @return int
		 */
		public function getHeight()
		{
			return imagesy($this->image);
		}

		/**
		 * Not implemented yet.
		 * @return int
		 */
		public function getPresentationTimestamp()
		{
			return 0;
		}

		/**
		 * Not implemented yet.
		 * @return int
		 */
		public function getPTS()
		{
			return $this->getPresentationTimestamp();
		}
		
		/**
		 * Returns a truecolor GD image of the frame.
		 * 
		 * @return resource		A gd image
		 */
		public function toGDImage()
		{
			return $this->image;
		}
	}


	/**
	 * A class compatible with ffmpeg_movie, using the command line ffmpeg tool.
	 */
	class lfm_Ffmpeg_Movie
	{
		private $pathname;
		private $persistent;

		private $duration = null;
		private $start = null;
		private $bitrate = null;
		private $videoCodec = null;
		private $videoFrameHeight = null;
		private $videoFrameWidth = null;
		private $videoFrameRate = null;
		private $videoPixelFormat = null;
		private $videoBitRate = null;
		private $audioCodec = null;
		private $audioSampleRate = null;
		private $audioChannels = null;
		private $audioBitRate = null;

		private $currentFrameNumber = 0;
		
		private $ffmpegExecutable = 'ffmpeg';


		/**
		 * Open a video or audio file and return it as an object.
		 * 
		 * @param string	$pathname		File path of video or audio file to open.
		 * @param bool		$persistent		Whether to open this media as a persistent resource. 
		 */
		public function __construct($pathname, $persistent)
		{
			$this->pathname = $pathname;
			$this->persistent = $persistent;

			if ('\\' === DIRECTORY_SEPARATOR) {
				$this->ffmpegExecutable = 'ffmpeg.exe';
			}


			$cmd = $this->ffmpegExecutable . ' -i "' . $pathname . '" 2>&1';
			
			$ffmpegInfoOutput = lfm_execCmd($cmd);
			if (!$ffmpegInfoOutput) {
				return;
			}

			$lines = explode("\n", $ffmpegInfoOutput);

			foreach ($lines as $line) {
				if (preg_match('/^\s+Duration:\s+([^,]+),\s+start:\s+([^,]+),\s+bitrate:\s+(.+)$/', $line, $matches)) {
					//  Duration: 00:07:16.0, start: 0.000000, bitrate: 79 kb/s
					$duration = $matches[1];
					// The returned video duration is in hh:mm:ss.uuuu format.
					list($h, $m, $s) = explode(':', $matches[1]);
					$this->duration = (int)$h * 3600 + (int)$m * 60 + (float)$s;
					$this->start = (float)$matches[2];
					$this->bitrate = $matches[3];
				} elseif (preg_match('/^\s+Stream [^:]+:\s+Video:\s+([^,]+),\s+([^,]+),\s+([^,]+)x([^,]+),(?:\s+([^,]+),)?\s+(.+) \w+/', $line, $matches)) {
					//  Stream #0.0: Video: theora, yuv420p, 720x400, 25.00 fps(r)
					//  Stream #0.0[0x1e0]: Video: mpeg2video, yuv420p, 720x384, 1150 kb/s, 25.00 fps(r)
					//  Stream #0.0: Video: mpeg4, yuv420p, 672x288 [PAR 1:1 DAR 7:3], 23.98 tb(r)
					$this->videoCodec = $matches[1];
					$this->videoPixelFormat = $matches[2];
					$this->videoFrameWidth = (int)$matches[3];
					$this->videoFrameHeight = (int)$matches[4];
					if (isset($matches[6])) {
						$this->videoBitRate = $matches[5];
						$this->videoFrameRate = (float)$matches[6];
					} else {
						$this->videoFrameRate = (float)$matches[5];
					}
				} elseif (preg_match('/^\s+Stream [^:]+:\s+Audio:\s+([^,]+),\s+([^,]+),\s+([^,]+),\s+(.+)$/', $line, $matches)) {
					//  Stream #0.1: Audio: vorbis, 48000 Hz, stereo, 80 kb/s
					$this->audioCodec = $matches[1];
					$this->audioSampleRate = $matches[2];
					$this->audioChannels = $matches[3];
					$this->audioBitRate = $matches[4];
				}
			}
		}


		/**
		 * Return the duration of a movie or audio file in seconds.
		 * 
		 * @return float
		 */
		public function getDuration()
		{
			return $this->duration;
		}


		/**
		 * Return the number of frames in a movie file.
		 *
		 * @return int
		 */
		public function getFrameCount()
		{
			return (int)($this->duration * $this->videoFrameRate);
		}


		/**
		 * Return the frame rate of a movie in fps.
		 * 
		 * @return float
		 */
		public function getFrameRate()
		{
			return $this->videoFrameRate;	
		}


		/**
		 * Return the path and name of the movie file or audio file.
		 * 
		 * @return string
		 */
		public function getFilename()
		{
			return $this->pathname;
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getComment()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getTitle()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getAuthor()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getCopyright()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getArtist()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getGenre()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getTrackNumber()
		{
			return '';
		}


		/**
		 * Not implemented yet.
		 * @return string
		 */
		public function getYear()
		{
			return '';
		}


		/**
		 * Return the height of the movie in pixels.
		 *  
		 * @return int
		 */
		public function getFrameHeight()
		{
			return $this->videoFrameHeight;
		}


		/**
		 * Return the width of the movie in pixels.
		 *  
		 * @return int
		 */
		public function getFrameWidth()
		{
			return $this->videoFrameWidth;			
		}


		/**
		 * Return the pixel format of the movie.
		 * 
		 * @return string
		 */
		public function getPixelFormat()
		{
			return $this->videoPixelFormat;
		}


		/**
		 * Return the bit rate of the movie or audio file in bits per second.
		 * 
		 * @return string
		 */
		public function getBitRate()
		{
			return $this->bitrate;
		}


		/**
		 * Return the bit rate of the video in bits per second.
		 * NOTE: This only works for files with constant bit rate.
		 * 
		 * @return string
		 */
		public function getVideoBitRate()
		{
			return $this->videoBitRate;
		}


		/**
		 * Return the audio bit rate of the media file in bits per second.
		 * 
		 * @return string
		 */
		public function getAudioBitRate()
		{
			return $this->audioBitRate;
		}


		/**
		 * Return the audio sample rate of the media file in bits per second.
		 * 
		 * @return float
		 */
		public function getAudioSampleRate()
		{
			return $this->audioSampleRate;
		}


		/**
		 * Return the current frame index.
		 * 
		 * @return int
		 */
		public function getFrameNumber()
		{
			return $this->currentFrameNumber;
		}


		/**
		 * Return the name of the video codec used to encode this movie as a string.
		 * 
		 * @return string
		 */
		public function getVideoCodec()
		{
			return $this->videoCodec;
		}


		/**
		 * Return the name of the audio codec used to encode this movie as a string.
		 * 
		 * @return string
		 */
		public function getAudioCodec()
		{
			return $this->audioCodec;	
		}


		/**
		 * Return the number of audio channels in this movie as an integer.
		 * 
		 * @return int
		 */
		public function getAudioChannels()
		{
			return $this->audioChannels;	
		}


		/**
		 * Return boolean value indicating whether the movie has an audio stream.
		 * 
		 * @return bool
		 */
		public function hasAudio()
		{
			return !is_null($this->audioChannels);
		}


		/**
		 * Returns a frame from the movie as an lfm_Ffmpeg_Frame object.
		 * Returns false if the frame was not found. 
		 * 
		 * @param int	$frameNumber	Frame from the movie to return. If no frameNumber is specified, returns the next frame of the movie. 
		 * @return lfm_Ffmpeg_Frame
		 */
		public function getFrame($frameNumber = null)
		{
			if (!isset($frameNumber)) {
				$frameNumber = $this->currentFrameNumber;
			}
			if ($frameNumber < 0 || $frameNumber > $this->getFrameCount()) {
				return false;
			}
			$nbSeconds = $frameNumber / $this->videoFrameRate;

			$nbHours = (int)($nbSeconds / 3600);
			$nbSeconds -= ($nbHours * 3600);
			$nbMinutes = (int)($nbSeconds / 60);
			$nbSeconds -= ($nbMinutes * 60);
			$position = sprintf('%02d:%02d:%04.2f', $nbHours, $nbMinutes, $nbSeconds);

			$addon = bab_getAddonInfosInstance('LibFileManagement');
			$tempImagePathname = $addon->getUploadPath() . 'temp.jpg';
			$cmd = $this->ffmpegExecutable . ' -y -i "'. $this->getFilename() .'" -f mjpeg -ss ' . $position . ' -vframes 1 -an "' . $tempImagePathname . '"';

			lfm_system($cmd, $retval);
			if ($retval !== 0) {
				return false;
			}

			$image = imageCreateFromJpeg($tempImagePathname);

			unlink($tempImagePathname);

			return new lfm_Ffmpeg_Frame($image);
		}


		/**
		 * Returns the next key frame from the movie as an lfm_Ffmpeg_Frame object.
		 * Returns false if the frame was not found.
		 * 
		 * @return lfm_Ffmpeg_Frame
		 */
		public function getNextKeyFrame()
		{
			return $this->getFrame($this->currentFrameNumber++);
		}
	}

}
